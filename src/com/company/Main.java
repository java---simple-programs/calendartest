package com.company;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Main {

    public static void main(String[] args) {
        GregorianCalendar d = new GregorianCalendar();
        //zainicjalizowac z dzisiejsza data
        int today = d.get(Calendar.DAY_OF_MONTH);
        int month = d.get(Calendar.MONTH);

        //d to poczatek miesiaca
        d.set(Calendar.DAY_OF_MONTH, 1);

        int dayOfWeek = d.get(Calendar.DAY_OF_WEEK);

        //drukowanie naglowka
        System.out.println("Sun Mon Tue Wed Thu Fri Sat");

        //puste pola pierwszej linii zapelnic spacjami
        for(int i=Calendar.SUNDAY; i<dayOfWeek; i++){
            System.out.println(" ");
        }

        do{
            int day = d.get(Calendar.DAY_OF_MONTH);
            if(day<){
                System.out.println(" "+day);
            }
            //aktualny dzien *

            if(day==today){
                System.out.println("*");
            }else {
                System.out.println(" ");
            }
            //po kazdej sobocie zacznij nowa linie

            if(dayOfWeek==Calendar.SATURDAY) {
                System.out.println();
            }
            //przesun d na nastepny dzien

            d.add(Calendar.DAY_OF_MONTH,1);
            dayOfWeek=d.get(Calendar.DAY_OF_WEEK);
        }
        while (d.get(Calendar.MONTH)==month);
        //petla konczy sie, gdy d staje sie pierwszym dniem nastepnego miesiaca

        //jesli trzeba wydrukuj ostatnia linie
        if(dayOfWeek != Calendar.SUNDAY){
            System.out.println();
        }
    }
}
